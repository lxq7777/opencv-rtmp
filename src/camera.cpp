#include "camera.hpp"

cv::VideoCapture get_device(char* camID, int width, int height)
{
    cv::VideoCapture cam(camID);
    if (!cam.isOpened())
    {
        std::cout << "Failed to open video capture device!" << std::endl;
        exit(1);
    }
    else
    {
        std::cout << "Successfully to open video capture device!" << std::endl;
    }

    // query setting height & width
    std::cout << "camera frame height:" << cam.get(3) << std::endl;
    std::cout << "camera frame weight:" << cam.get(4) << std::endl;

    // cam.set(cv::CAP_PROP_FRAME_WIDTH, width);
    // cam.set(cv::CAP_PROP_FRAME_HEIGHT, height);

    return cam;
}

