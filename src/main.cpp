#include "main.hpp"

void signal_handler(int signal)
{
    if (signal == SIGINT)
    {
        cout << "receive signal SIGINT,Ready to quit safely" << endl;
        exit(0);
    }
}

// rtmp_address, camera_path, camera_width, camera_height, camera_frame_rate, camera_bit_rate, camera_frame_quality
int main(int argc, char *argv[])
{
    // camera_av_stream av_instance = camera_av_stream("/home/pi/Desktop/opencv-rtmp/out1.avi");
    // camera_av_stream av_instance = camera_av_stream("rtmp://live-push.bilivideo.com/live-bvc/?streamname=live_27296992_21346559&key=bb2fe41f7dbd5c4a3647d9813ca76e4d&schedule=rtmp&pflag=1");
    if (argc != 8)
    {
        printf("Wrong argument number !\n");
        return 1;
    }

    DEFAULT_CAMERA_WIDTH = atoi(argv[3]);
    DEFAULT_CAMERA_HEIGHT = atoi(argv[4]);
    DEFAULT_CAMERA_FPS = atoi(argv[5]);
    DEFAULT_CAMERA_BITRATE = atoi(argv[6]);
    // sprintf(DEFAULT_FRAME_QUALITY,"%s",argv[7]);

    printf("Process ID -> %d,push stream to -> %s,cameraPATH -> %s,camera_width -> %d,camera_height -> %d,camera_frame_rate -> %d,camera_bit_rate -> %d,camera_frame_quality -> %s\n", getpid(), argv[1], argv[2], DEFAULT_CAMERA_WIDTH, DEFAULT_CAMERA_HEIGHT, DEFAULT_CAMERA_FPS, DEFAULT_CAMERA_BITRATE, DEFAULT_FRAME_QUALITY);

    signal(SIGINT, signal_handler);

    camera_av_stream av_instance = camera_av_stream(argv[1],argv[7]);

    auto cam = get_device(argv[2], atoi(argv[3]), atoi(argv[4]));
    av_instance.av_wrtie_stream(cam);

    return 0;
}