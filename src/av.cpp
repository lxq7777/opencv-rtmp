#include "av.hpp"

camera_av_stream::camera_av_stream(std::string server,char* quality)
{
    set_server(server);
    av_init(quality);
}

camera_av_stream::~camera_av_stream()
{
}

void camera_av_stream::set_server(std::string server)
{
    camera_av_stream::server = server;
}

void camera_av_stream::av_init(char* quality)
{
    dump_log = true;
    if (dump_log)
    {
        av_log_set_level(AV_LOG_DEBUG);
    }

#if LIBAVCODEC_VERSION_INT < AV_VERSION_INT(58, 9, 100)
    av_register_all();
#endif
    // openssl 加密传输
    avformat_network_init();

    // 设置视频流格式
    initialize_avformat_context(ofmt_ctx, "flv");

    const char *output = server.c_str();
    // 设置输出流位置（目标地址）
    initialize_io_context(ofmt_ctx, output);

    // 查找H264编解码器
    out_codec = avcodec_find_encoder(AV_CODEC_ID_H264);
    // 创建流通道（H264视频流）
    out_stream = avformat_new_stream(ofmt_ctx, out_codec);
    // 为解码器分配内存
    out_codec_ctx = avcodec_alloc_context3(out_codec);
    // 设置编解码器参数
    set_codec_params(ofmt_ctx, out_codec_ctx, DEFAULT_CAMERA_WIDTH, DEFAULT_CAMERA_HEIGHT, DEFAULT_CAMERA_FPS, DEFAULT_CAMERA_BITRATE);

    // high444 无损
    // 设置压缩视频参数
    initialize_codec_stream(out_stream, out_codec_ctx, out_codec, quality);

    // 其他数据复制
    out_stream->codecpar->extradata = out_codec_ctx->extradata;
    out_stream->codecpar->extradata_size = out_codec_ctx->extradata_size;

    // 打印编解码参数
    av_dump_format(ofmt_ctx, 0, output, 1);
}

void camera_av_stream::av_wrtie_stream(cv::VideoCapture cam)
{

    swsctx = initialize_sample_scaler(out_codec_ctx, DEFAULT_CAMERA_WIDTH, DEFAULT_CAMERA_HEIGHT);
    frame = allocate_frame_buffer(out_codec_ctx, DEFAULT_CAMERA_WIDTH, DEFAULT_CAMERA_HEIGHT);

    // 写入帧头
    int ret = avformat_write_header(ofmt_ctx, nullptr);
    if (ret < 0)
    {
        std::cout << "Could not write header!" << std::endl;
        exit(1);
    }

    // 设置摄像头获取帧数据内存空间
    std::vector<uint8_t> imgbuf(640 * 480 * 3 + 16);

    cv::Mat image(640, 480, CV_8UC3, imgbuf.data(), 640 * 3);
    bool end_of_stream = false;

    printf("Ready to send stream\n");
    do
    {
    // for (int i = 0; i < 200; i++)
    // {
        cam >> image;
        // cv::imshow("pic",image);
        // cv::waitKey(0);

        const int stride[] = {static_cast<int>(image.step[0])};
        // 拉伸数据
        sws_scale(swsctx, &image.data, stride, 0, image.rows, frame->data, frame->linesize);
        // 时间基拉伸
        frame->pts += av_rescale_q(1, out_codec_ctx->time_base, out_stream->time_base);
        // 写入帧数据
        write_frame(out_codec_ctx, ofmt_ctx, frame);
    // }

    } while (!end_of_stream);
    // 写入帧尾
    av_write_trailer(ofmt_ctx);

    av_frame_free(&frame);
    avcodec_close(out_codec_ctx);
    avio_close(ofmt_ctx->pb);
    // avformat_free_context(ofmt_ctx);
}

void camera_av_stream::initialize_avformat_context(AVFormatContext *&fctx, const char *format_name)
{
    int ret = avformat_alloc_output_context2(&fctx, nullptr, format_name, nullptr);
    if (ret < 0)
    {
        std::cout << "Could not allocate output format context!" << std::endl;
        exit(1);
    }
}

void camera_av_stream::initialize_io_context(AVFormatContext *&fctx, const char *output)
{
    if (!(fctx->oformat->flags & AVFMT_NOFILE))
    {
        int ret = avio_open2(&fctx->pb, output, AVIO_FLAG_WRITE, nullptr, nullptr);
        if (ret < 0)
        {
            std::cout << "Could not open output IO context!" << std::endl;
            exit(1);
        }
    }
}

void camera_av_stream::set_codec_params(AVFormatContext *&fctx, AVCodecContext *&codec_ctx, int width, int height, int fps, int bitrate)
{
    // 设置帧速率
    const AVRational dst_fps = {fps, 1};

    codec_ctx->codec_tag = 0;
    codec_ctx->codec_id = AV_CODEC_ID_H264;
    codec_ctx->codec_type = AVMEDIA_TYPE_VIDEO;
    codec_ctx->width = width;
    codec_ctx->height = height;
    // I/P/B 连续组大小
    codec_ctx->gop_size = 25;
    // 像素点格式YUV
    codec_ctx->pix_fmt = AV_PIX_FMT_YUV420P;
    codec_ctx->framerate = dst_fps;
    // 设置时间基
    codec_ctx->time_base = av_inv_q(dst_fps);
    codec_ctx->bit_rate = bitrate;
    // 设置预览第一帧
    if (fctx->oformat->flags & AVFMT_GLOBALHEADER)
    {
        codec_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    }
}

void camera_av_stream::initialize_codec_stream(AVStream *&stream, AVCodecContext *&codec_ctx, AVCodec *&codec, std::string codec_profile)
{
    // 复制参数（编解码器->流通道）
    int ret = avcodec_parameters_from_context(stream->codecpar, codec_ctx);
    if (ret < 0)
    {
        std::cout << "Could not initialize stream codec parameters!" << std::endl;
        exit(1);
    }

    AVDictionary *codec_options = nullptr;
    // 视频清晰度设置（画质）
    av_dict_set(&codec_options, "profile", codec_profile.c_str(), 0);
    // 编码速度与质量
    av_dict_set(&codec_options, "preset", "ultrafast", 0);
    // 零延迟编码
    av_dict_set(&codec_options, "tune", "zerolatency", 0);

    // 启用编解码器
    ret = avcodec_open2(codec_ctx, codec, &codec_options);
    if (ret < 0)
    {
        std::cout << "Could not open video encoder!" << std::endl;
        exit(1);
    }
}

SwsContext *camera_av_stream::initialize_sample_scaler(AVCodecContext *codec_ctx, int width, int height)
{
    // 拉伸容器设置
    // 原图像宽,原图像高，原图像像素格式，输出图像宽，输出图像高，输出像素格式，拉伸图像算法
    SwsContext *swsctx = sws_getContext(width, height, AV_PIX_FMT_BGR24, width, height, codec_ctx->pix_fmt, SWS_BILINEAR, nullptr, nullptr, nullptr);
    if (!swsctx)
    {
        std::cout << "Could not initialize sample scaler!" << std::endl;
        exit(1);
    }

    return swsctx;
}

AVFrame *camera_av_stream::allocate_frame_buffer(AVCodecContext *codec_ctx, int width, int height)
{
    // 分配帧内存空间
    AVFrame *frame = av_frame_alloc();

    std::vector<uint8_t> framebuf(av_image_get_buffer_size(codec_ctx->pix_fmt, width, height, 1));
    // 将内存划分
    av_image_fill_arrays(frame->data, frame->linesize, framebuf.data(), codec_ctx->pix_fmt, width, height, 1);
    // 设置帧宽/高/像素格式
    frame->width = width;
    frame->height = height;
    frame->format = static_cast<int>(codec_ctx->pix_fmt);

    return frame;
}

void camera_av_stream::write_frame(AVCodecContext *codec_ctx, AVFormatContext *fmt_ctx, AVFrame *frame)
{
    AVPacket pkt = {0};
    av_new_packet(&pkt, 0);

    // 将帧数据送入编解码器
    int ret = avcodec_send_frame(codec_ctx, frame);
    if (ret < 0)
    {
        std::cout << "Error sending frame to codec context!" << std::endl;
        exit(1);
    }
    // 从编解码器获取输出数据
    ret = avcodec_receive_packet(codec_ctx, &pkt);
    if (ret < 0)
    {
        std::cout << "Error receiving packet from codec context!" << std::endl;
        exit(1);
    }

    // 写入帧数据
    av_interleaved_write_frame(fmt_ctx, &pkt);
    // 擦除数据包
    av_packet_unref(&pkt);
}