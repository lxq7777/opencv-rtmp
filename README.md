# 实现功能
## 摄像头设备查询
## 摄像头注册
## 摄像头下线检测与上报
## 开启摄像头推流
## 摄像头参数获取与设置

# 构建方法
```
git clone https://gitlab.com/lxq7777/opencv-rtmp
mkdir build
cd ./build
cmake ..
sudo make -j3
```
# 实验平台
## 树莓派4B