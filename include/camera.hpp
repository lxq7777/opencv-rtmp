#ifndef __CAMERA_HPP__
#define __CAMERA_HPP__

#include "main.hpp"

#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>


cv::VideoCapture get_device(char* camID, int width, int height);



#endif