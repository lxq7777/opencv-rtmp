#ifndef __AV_HPP__
#define __AV_HPP__

#include "main.hpp"
#include "camera.hpp"
#include "option.hpp"

#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>

extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>

}

using namespace std;

#pragma once
class camera_av_stream
{
private:
    AVFormatContext *ofmt_ctx = nullptr;
    AVCodec *out_codec = nullptr;
    AVStream *out_stream = nullptr;
    AVCodecContext *out_codec_ctx = nullptr;
    AVFrame *frame = nullptr;
    SwsContext *swsctx = nullptr;
    std::string server = "";

public:
    camera_av_stream(std::string server,char* quality);
    ~camera_av_stream();
    void av_init(char* quality);
    void initialize_avformat_context(AVFormatContext *&fctx, const char *format_name);
    void initialize_io_context(AVFormatContext *&fctx, const char *output);
    void set_codec_params(AVFormatContext *&fctx, AVCodecContext *&codec_ctx, int width, int height, int fps, int bitrate);
    void initialize_codec_stream(AVStream *&stream, AVCodecContext *&codec_ctx, AVCodec *&codec, std::string codec_profile);
    SwsContext *initialize_sample_scaler(AVCodecContext *codec_ctx, int width, int height);
    AVFrame *allocate_frame_buffer(AVCodecContext *codec_ctx, int width, int height);
    void write_frame(AVCodecContext *codec_ctx, AVFormatContext *fmt_ctx, AVFrame *frame);
    void av_wrtie_stream(cv::VideoCapture cam);
    void set_server(std::string server);
};

#endif