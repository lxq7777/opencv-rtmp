#ifndef __MAIN_H__
#define __MAIN_H__

#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <unistd.h>
#include <signal.h>

extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

#include "camera.hpp"
#include "av.hpp"

// static int DEFAULT_CAMERA_ID = 0;
static int DEFAULT_CAMERA_FPS = 30;
static int DEFAULT_CAMERA_WIDTH = 640;
static int DEFAULT_CAMERA_HEIGHT = 480;
static int DEFAULT_CAMERA_BITRATE = 500000;
static char DEFAULT_FRAME_QUALITY[20];

static bool dump_log = false;

#endif